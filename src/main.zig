const std = @import("std");
const rl = @import("raylib");

// on alloc failure, increase size of this buffer
var _scrap_buffer: [4096]u8 = undefined;
var _a_scrap = std.heap.FixedBufferAllocator.init(&_scrap_buffer);
const a_scrap = _a_scrap.allocator();

// persistent & unchanging
var _static_buffer: [4096]u8 = undefined;
var _a_static = std.heap.FixedBufferAllocator.init(&_static_buffer);
const a_static = _a_static.allocator();

const Mine = struct {
    ores_left: usize,
};

const Tool = enum {
    pickaxe,
    fn describe(this: @This()) [:0]const u8 {
        return @tagName(this);
    }
};

const Building = union(enum) {
    Mine: Mine,
};

const WorldCoords = struct {
    // which strip?
    id: usize,
    // which location on strip?
    loc: usize,
};

const WorldLocation = struct {
    portal: ?WorldCoords = null,
    building: ?Building = null,
    tool: ?Tool = null,

    pub fn describe(this: @This()) ![:0]const u8 {
        var text = std.ArrayList(u8).init(a_scrap);

        if (this.building) |building| {
            switch (building) {
                .Mine => |mine| try text.writer().print("A Mine with {} ores left. ", .{mine.ores_left}),
            }
        }

        if (this.tool) |tool|
            try text.writer().print("On the floor: {s}. ", .{tool.describe()});

        if (this.portal) |location|
            try text.writer().print("Portal to ({}, {}). ", .{ location.id, location.loc });

        if (text.items.len == 0) return "Nothing here";

        return try text.toOwnedSliceSentinel(0);
    }
};

const WorldStrip = struct {
    name: []const u8,
    locations: []WorldLocation,
    fn describe(this: @This()) []const u8 {
        return this.name;
    }
};

fn build_demo_map() ![]WorldStrip {
    return try a_static.dupe(WorldStrip, &.{
        .{ .name = "Ore Mine", .locations = try a_static.dupe(WorldLocation, &.{
            .{ .portal = .{ .id = 1, .loc = 2 } },
            .{ .building = .{ .Mine = .{ .ores_left = 10 } } },
            .{},
            .{},
            .{ .tool = .pickaxe },
        }) },
        .{ .name = "TK", .locations = try a_static.dupe(WorldLocation, &.{
            .{},
            .{},
            .{ .portal = .{ .id = 0, .loc = 0 } },
            .{},
            .{},
        }) },
    });
}

const GameState = struct {
    map: []WorldStrip,
    pos: WorldCoords = .{ .id = 0, .loc = 0 },
    tool_equipped: ?Tool = null,

    fn demo() !@This() {
        return .{ .map = try build_demo_map() };
    }

    fn current_strip(this: *@This()) *WorldStrip {
        return &this.map[this.pos.id];
    }

    fn current_loc(this: *@This()) *WorldLocation {
        return &this.current_strip().locations[this.pos.loc];
    }

    fn move_left_maybe(this: *@This()) void {
        if (this.pos.loc != 0) this.pos.loc -= 1;
    }
    fn move_right_maybe(this: *@This()) void {
        if (this.pos.loc < this.current_strip().locations.len - 1) this.pos.loc += 1;
    }
    fn do_action(this: *@This()) void {
        const loc = this.current_loc();

        // portal
        if (loc.portal) |pos| {
            this.pos = pos;
            return;
        }

        // mine
        if (loc.building) |*building| {
            switch (building.*) {
                .Mine => |*mine| {
                    if (this.tool_equipped == .pickaxe and mine.ores_left > 0) {
                        mine.ores_left -= 1;
                        return;
                    }
                },
            }
        }

        // pick up or drop tool
        if (loc.tool != null or this.tool_equipped != null) {
            std.mem.swap(?Tool, &loc.tool, &this.tool_equipped);
            return;
        }
    }
};

pub fn main() !void {
    rl.InitWindow(800, 800, "hello world!");
    rl.SetConfigFlags(rl.ConfigFlags{ .FLAG_WINDOW_RESIZABLE = true });
    rl.SetTargetFPS(60);
    defer rl.CloseWindow();

    var this = try GameState.demo();

    while (!rl.WindowShouldClose()) {
        _a_scrap.reset();

        // poll events
        rl.BeginDrawing();
        defer rl.EndDrawing();

        // update
        if (rl.IsKeyPressed(.KEY_LEFT)) this.move_left_maybe();
        if (rl.IsKeyPressed(.KEY_RIGHT)) this.move_right_maybe();
        if (rl.IsKeyPressed(.KEY_A) or rl.IsKeyPressed(.KEY_SPACE)) this.do_action();

        // draw
        rl.ClearBackground(rl.BLACK);
        rl.DrawFPS(10, 10);

        for (0.., this.current_strip().locations) |i, loc| {
            rl.DrawRectangleLines(100 + @as(i32, @intCast(i)) * 30, 200, 32, 32, rl.YELLOW);
            _ = loc;
            if (i == this.pos.loc) rl.DrawRectangle(100 + @as(i32, @intCast(i)) * 30, 200, 32, 32, rl.YELLOW);
        }

        const loc = this.current_loc();

        const loc_text = try std.fmt.allocPrintZ(a_scrap, "Strip {}: {s}", .{ this.pos.id, this.current_strip().describe() });
        rl.DrawText(loc_text, 100, 140, 20, rl.YELLOW);
        rl.DrawText(try loc.describe(), 100, 170, 20, rl.YELLOW);

        // player text
        const tool_text = try std.fmt.allocPrintZ(a_scrap, "Tool: {s}", .{if (this.tool_equipped) |tool| tool.describe() else "None"});
        rl.DrawText(tool_text, 100, 250, 20, rl.YELLOW);
    }
}
