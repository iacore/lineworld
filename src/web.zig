const std = @import("std");
const rl = @import("raylib");
const emsdk = @cImport({
    @cDefine("__EMSCRIPTEN__", "1");
    @cDefine("PLATFORM_WEB", "1");
    @cInclude("emscripten/emscripten.h");
});

////special entry point for Emscripten build, called from src/marshall/emscripten_entry.c
export fn emsc_main() callconv(.C) c_int {
    @import("main.zig").main() catch |err| {
        std.log.err("ERROR: {?}", .{err});
        return 1;
    };
    return 0;
}

export fn emsc_set_window_size(width: c_int, height: c_int) callconv(.C) void {
    rl.SetWindowSize(@as(i32, @intCast(width)), @as(i32, @intCast(height)));
}
